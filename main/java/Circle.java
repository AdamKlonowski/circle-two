import java.util.Scanner;

public class Circle {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj średnicę okręgu:");

        float s = sc.nextFloat();

        float pi = (float) 3.14;
        float pim = (float) Math.PI;

        float result1 = s * pi;
        float result2 = s * pim;

        System.out.println("Obwód koła: " + result1 + "   " + result2);


    }

}
